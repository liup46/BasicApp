鉴于某些原因（中美脱钩....hhhha）后续主要更新在gitee上 https://gitee.com/liup46/BasicApp

## 功能：
1. Log日志库：logan 
2. 路由暂时暂时使用：Aroute
3. 网络库：okhttp+coroutines+Gson
4. 渠道包功能：Walle
5. crash监控功能：xcrash
6. UI + 范式: 基本完成了类mobx方式的属性绑定, 请求与ui的绑定 ✔︎
    1. view 支持corner radius, shape, stroke ✔︎
    2. 支持iconfonnt 自动生成string脚本 ✔︎
    3. Toolbar ✔︎
    4. StateView ✔︎
    5. imageView, Glide ->ing
    6. BottomNavBar
    7. 富文本
    8. 列表框架
    9. Tab
    10. BANNER
    11. 通用dialog(DialogFragment✔︎,  Alert ✔︎， popup)
    12. toast
    13. 动画
    14. 水平滚动选择缩放列表HorizontalSelectListView ✔︎
    15. simple DanmuGroupView ✔︎
    .....

## TODO:
1. 登录业务(测试上面的ui +范式)
2. 调试工具
3. 扫二维码
4. lint
5. 插件化: 方案：Phantom
6. Flutter
7. 热更新
8. react