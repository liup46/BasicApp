package com.basic.net

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import org.junit.Test
import java.io.IOException
import java.util.*

/**
 * @author Peter Liu
 * @since 2023/3/21 22:13
 *
 */
class TestNet {


    @Test
    fun test() {
        GlobalScope.launch {
            request {
                HttpService.get<String>(path = "/asdb")
                HttpService.get<String>("", "")
                HttpService.post<String>(path = "111", body = "{}")
            }.onData {

            }.onFailed(true)
        }
    }

    @Test
    fun testWithContext(){
        println("000:" + Thread.currentThread().id + " ," + Thread.currentThread())

        runBlocking {
            println("111:" + Thread.currentThread().id + " ," + Thread.currentThread())
            val rel =  withContext(Dispatchers.IO) {
                println("22:" + Thread.currentThread().id + " ," + Thread.currentThread())
                "abc"
            }
            println("333:" + Thread.currentThread().id + " ," + Thread.currentThread())
            println("333:"+rel)
        }
    }

    @Test
    fun testFlow() {
        println("000:" + Thread.currentThread().id + " ," + Thread.currentThread())
        runBlocking {
            flow<String> {
                println("111:" + Thread.currentThread().id + " ," + Thread.currentThread())
                for (i in 1..10) {
                    if (i == 5) {
                        throw java.lang.IllegalArgumentException("error $i")
                    }
                    emit("$i")
                }
            }.flowOn(Dispatchers.IO)
                .onCompletion {
                    println("222:" + Thread.currentThread().id + " ," + Thread.currentThread())
                    println(it)
                }.catch {
                    println("444:" + Thread.currentThread().id + " ," + Thread.currentThread())
                    println(it)
                }.collectLatest {
                    println("333: " + Thread.currentThread().id + " ," + Thread.currentThread())
                    println("333: " + it)
                }
//                .collect {
//                    println("333: " + Thread.currentThread().id + " ," + Thread.currentThread())
//                    println("333: " + it)
//                }
        }
    }

    @Test
    fun TEST2() = runBlocking {
        val job = GlobalScope.launch { // launch 根协程
            println("Throwing exception from launch")
            throw IndexOutOfBoundsException() // 我们将在控制台打印 Thread.defaultUncaughtExceptionHandler
        }
        job.join()
        println("Joined failed job")
        val deferred = GlobalScope.async { // async 根协程
            println("Throwing exception from async")
            throw ArithmeticException() // 没有打印任何东西，依赖用户去调用等待
        }
        try {
            deferred.await()
            println("Unreached")
        } catch (e: ArithmeticException) {
            println("Caught ArithmeticException")
        }
    }


    @Test
    fun test3() = runBlocking {
        val handler = CoroutineExceptionHandler { _, exception ->
            println("CoroutineExceptionHandler got $exception with suppressed ${exception.suppressed.contentToString()}")
        }
        val job = GlobalScope.launch(handler) {
            launch {
                try {
                    delay(Long.MAX_VALUE) // 当另一个同级的协程因 IOException  失败时，它将被取消
                } finally {
                    throw ArithmeticException() // 第二个异常
                }
            }
            launch {
                delay(100)
                throw IOException() // 首个异常
            }
            delay(Long.MAX_VALUE)
        }
        job.join()
        println("Joined failed job")

    }


}