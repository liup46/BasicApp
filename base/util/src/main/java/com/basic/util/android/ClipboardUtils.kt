package com.basic.util.android

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context

/**
 * @author Peter Liu
 * @since 2023/8/15 14:02
 *
 */

fun Context.getClipText(): CharSequence {
    val clipService = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    return clipService.primaryClip?.let {
        return if (it.itemCount > 0) {
            it.getItemAt(it.itemCount - 1).text
        } else {
            ""
        }
    } ?: ""
}

fun Context.setClipText(charSequence: CharSequence) {
    val clipService = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    val clipdata = ClipData.newPlainText(charSequence, charSequence)
    clipService.setPrimaryClip(clipdata)
}
