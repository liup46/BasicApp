#!/usr/bin/env bash


########################################
#打渠道包脚本:
#1.首先编译出apk, 例如执行./gradlew clean assembleChinaRelease, 注意只需编译china flavor 的渠道包
#2.执行 ./package.sh, 完成后生成的渠道包在 app/build/outputs/apk/china/release/channel 目录下
########################################


root_path=$(cd $(dirname $0); pwd)
echo $root_path
#chmod a+x ./gradlew
#./gradlew clean assembleChinaRelease
java -jar walle-cli-all.jar batch -f $root_path/channel $root_path/app/build/outputs/apk/china/release/app-china-release.apk $root_path/app/build/outputs/apk/china/release/channel