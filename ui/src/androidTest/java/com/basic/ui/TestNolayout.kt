package com.basic.ui

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.basic.ui.view.*

/**
 * @author Peter Liu
 * @since 2023/7/20 23:21
 *
 */
class TestNolayout {

    fun test(context1: Context): View {
        return ConstraintLayout(context1) {
            val text1 = TextView {
                layoutParams = this@ConstraintLayout.Params {
                    width = ViewGroup.LayoutParams.MATCH_PARENT
                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    topMargin = 12.dp
                }
                text = "123"
                setTextColor(R.color.black.resColor())
            }
            val text2 = TextView {
                text = "456"
                layoutParams = this@ConstraintLayout.Params {
                    topToBottom = text1.id
                    startToStart = text1.id
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    bottomToBottom = gNextId()
                    topMargin = 12.dp
                }
                setTextColor(R.color.black.resColor())
                setOnClickListener {
                    println("456")
                }
            }

            FrameLayout {
                TextView {
                    layoutParams = this@FrameLayout.Params {
                        marginStart = 12.dp
                    }
                    text = "!23"
                }
            }

            //这个带context 的构造函数只创建view, 要手动add
            val image = ImageView(context) {
                setImageResource(androidx.appcompat.R.drawable.tooltip_frame_light)
            }
            add(image)
        }
    }

}