package com.basic.ui

/**
 * @author Peter Liu
 * @since 2023/7/14 01:38
 *
 */

@Retention(AnnotationRetention.SOURCE)
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.FILE,
    AnnotationTarget.CLASS
)
annotation class UnTested(val msg: String = "This is Untest")

