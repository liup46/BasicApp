package com.frogsing.libutil

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.Uri

object ThirdPartyAppUtils {
    /**
     * 用户是否安装微信客户端
     */
    fun isWeiXinInstalled(context: Context): Boolean {
        return isPackageInstalled(context, "com.tencent.mm")
    }

    /**
     * 用户是否安装QQ客户端
     */
    fun isQQInstalled(context: Context): Boolean {
        return isPackageInstalled(context, "com.tencent.mobileqq") ||
                isPackageInstalled(context, "com.tencent.qqlite")
    }

    /**
     * 用户是否安装微博
     */
    fun isWeiBoInstalled(context: Context): Boolean {
        return isPackageInstalled(context, "com.sina.weibo")
    }

    /**
     * 用户是否安抖音
     */
    fun isDouYinInstalled(context: Context): Boolean {
        return isPackageInstalled(context, "com.ss.android.ugc.aweme")
    }

    fun isPackageInstalled(context: Context, pack: String): Boolean {
        val packageManager: PackageManager = context.packageManager
        val infos: List<PackageInfo> = packageManager.getInstalledPackages(0)
        for (element in infos) {
            val pn: String = element.packageName
            if (pn.equals(pack, ignoreCase = true)) {
                return true
            }
        }
        return false
    }

    fun gotoWechat(context: Context) {
        try {
            val intent = Intent(Intent.ACTION_MAIN)
            val cmp = ComponentName("com.tencent.mm", "com.tencent.mm.ui.LauncherUI")
            intent.addCategory(Intent.CATEGORY_LAUNCHER)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.component = cmp
            context.startActivity(intent)
        } catch (_: Exception) {
        }
    }

    fun gotoQQ(context: Context) {
        runCatching {
            launchQQ(context)
        }.recoverCatching {
            launchQQByUri(context)
        }.recoverCatching {
        }
    }

    private fun launchQQ(context: Context) {
        val intent = Intent(Intent.ACTION_MAIN)
        val cmp = ComponentName(
            "com.tencent.mobileqq",
            "com.tencent.mobileqq.activity.SplashActivity"
        )
        intent.addCategory(Intent.CATEGORY_LAUNCHER)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.component = cmp
        context.startActivity(intent)
    }

    private fun launchQQByUri(context: Context) {
        //指定的QQ号只需要修改uin后的值即可。
        val url = "mqqwpa://im/chat?chat_type=wpa&uin=10000"
        context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
    }

    fun gotoDouYin(context: Context) {
        runCatching {
            launchDouYin(context)
        }.recoverCatching {
            launchDouYinLite(context)
        }.recoverCatching {
            launchDouYinByUri(context)
        }.recoverCatching {
        }
    }

    private fun launchDouYin(context: Context) {
        val intent = Intent(Intent.ACTION_MAIN)
        val cmp = ComponentName(
            "com.ss.android.ugc.aweme",
            "com.ss.android.ugc.aweme.main.MainActivity"
        )
        intent.addCategory(Intent.CATEGORY_LAUNCHER)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.component = cmp
        context.startActivity(intent)
    }

    private fun launchDouYinLite(context: Context) {
        val intent = Intent(Intent.ACTION_MAIN)
        val cmp = ComponentName(
            "com.ss.android.ugc.aweme.lite",
            "com.ss.android.ugc.aweme.main.MainActivity"
        )
        intent.addCategory(Intent.CATEGORY_LAUNCHER)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.component = cmp
        context.startActivity(intent)
    }

    private fun launchDouYinByUri(context: Context) {
        val intent = Intent(Intent.ACTION_VIEW)
        // 93325972684 这个是抖音中的userID 不是抖音号!!
        intent.data = Uri.parse("snssdk1128://aweme/detail/dyfuwu666")
        context.startActivity(intent)
    }
}