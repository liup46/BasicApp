package com.basic.ui

import android.content.Context
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.util.TypedValue
import androidx.annotation.FontRes
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.basic.env.App
import kotlin.math.roundToInt


/**
 * 资源转换类
 *
 */
fun Int.resColor(): Int = ContextCompat.getColor(App.getContext(), this)

fun Int.resString(): String = App.getContext().getString(this)

fun Int.resString(vararg args: Any): String = App.getContext().getString(this, *args)

fun Int.resDrawable(): Drawable? = ContextCompat.getDrawable(App.getContext(), this)

fun Int.resStringArray(): Array<String>? = App.getContext().resources?.getStringArray(this)

fun Int.resIntArray(): IntArray? = App.getContext().resources?.getIntArray(this)

/**
 * return resource dp to px,px to px
 */
fun Int.resPx(): Int = App.getContext().resources?.getDimensionPixelSize(this) ?: 0

/**
 * return resource px to dp, dp to dp
 */
fun Int.resDp(): Float = App.getContext().resources?.getDimension(this) ?: 0f
fun Int.resFont(assetFontFileName: String? = null): Typeface? =
    App.getContext().getFont(this, assetFontFileName)


/**
 * change dp number to px, return is px size
 *
 */
val Number.dp: Int
    get() {
        return this.toFloat().dp2px()
    }

internal fun Float.dp2px(): Int {
    val context = App.getContext()
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        this,
        context.resources.displayMetrics
    ).roundToInt()
}

internal fun Float.px2dp(): Float {
    val density = App.getContext().resources.displayMetrics.density
    return this / density
}

internal fun Context.getFont(@FontRes fontId: Int, fontFileName: String? = null): Typeface? {
    // 文字默认字体 android roboto medium字体
    return try {
        //在小米/oppo 某些手机crash：Resources$NotFoundException: Font resource ID could not be retrieved
        ResourcesCompat.getFont(this, fontId)
    } catch (e: Exception) {
        if (!fontFileName.isNullOrBlank()) {
            Typeface.createFromAsset(this.assets, fontFileName)
        } else {
            null
        }
    }
}



