package com.basic.ui.utils

import android.R
import android.content.Context
import android.graphics.Point
import android.os.Build
import android.util.TypedValue
import android.view.WindowManager
import com.basic.env.App
import com.basic.ui.resPx

/**
 * 屏幕相关的api
 *
 * @author Peter Liu
 * @since 2023/7/17 00:24
 *
 */

/**
 * 获取屏幕高度
 */
val Context.screenWidth: Int
    get() {
        val wm = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            wm.currentWindowMetrics.bounds.width()
        } else {
            return Point().apply {
                wm.defaultDisplay.getRealSize(this)
            }.x
        }
    }

/**
 * 获取屏幕高度
 */
val Context.screenHeight: Int
    get() {
        val wm = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            wm.currentWindowMetrics.bounds.height()
        } else {
            Point().apply {
                wm.defaultDisplay.getRealSize(this)
            }.y
        }
    }

object ScreenUtil {

    /**
     * 获取ActionBar高度
     *
     */
    val actionBarHeight by lazy {
        val context = App.getContext()
        val tv = TypedValue()
        if (context.theme.resolveAttribute(R.attr.actionBarSize, tv, true)) {
            TypedValue.complexToDimensionPixelSize(
                tv.data, context.resources.displayMetrics
            )
        }
        0
    }

    val defaultScreenWidth by lazy {
        App.getContext().screenWidth
    }

    val defaultScreenHight by lazy {
        App.getContext().screenHeight
    }

    val navigateBarHight by lazy {
        // 小米4没有nav bar, 而 navigation_bar_height 有值
        App.getContext().resources.getIdentifier(
            "navigation_bar_height", "dimen", "android"
        ).resPx()
    }

}
