package com.basic.ui.anim

import com.basic.ui.R

/**
 * @author Peter Liu
 * @since 2023/8/8 22:51
 *
 */

object WindowAnim {
    val BOTTOM_STYLE = R.style.BasicUI_Window_Anim_Bottom_Style

    val LEFT_STYLE = R.style.BasicUI_Window_Anim_Left_Style

    val CENTER_SCALE = R.style.BasicUI_Window_Anim_CenterScale_Style

    val RIGHT_STYLE = R.style.BasicUI_Window_Anim_Right_Style

    val TOP_STYLE = R.style.BasicUI_Window_Anim_Top_Style
}