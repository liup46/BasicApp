package com.basic.ui.anim

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ObjectAnimator
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.Animation
import android.view.animation.Animation.AnimationListener
import android.view.animation.DecelerateInterpolator
import android.view.animation.ScaleAnimation
import android.view.animation.TranslateAnimation

/**
 * @author Peter Liu
 * @since 2023/8/10 23:35
 *
 */
object Anim {

    internal const val ROTATE_ANIM_DURATION = 500L

    /**
     * 翻转view
     * @param autoBackDelay >0 表示多少ms之后复位
     */
    public fun rotate(foreView: View, backView: View, autoBackDelay: Long = -1) {
        backView.isClickable = false
        foreView.isClickable = false
        val foreRotator = ObjectAnimator.ofFloat(
            foreView, "rotationY", 0f, 90f
        )
        foreRotator.duration = ROTATE_ANIM_DURATION
        foreRotator.interpolator = AccelerateInterpolator()
        val backRotator = ObjectAnimator.ofFloat(
            backView, "rotationY", -90f, 0f
        )
        backRotator.duration = ROTATE_ANIM_DURATION
        backRotator.interpolator = DecelerateInterpolator()
        backRotator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
                if (autoBackDelay > 0) {
                    backView.postDelayed({
                        rotate(backView, foreView)
                    }, autoBackDelay)
                }
            }
        })
        foreRotator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(anim: Animator) {
                foreView.visibility = View.GONE
                backRotator.start()
                backView.visibility = View.VISIBLE
            }
        })
        foreRotator.start()
    }

    fun transation(
        view: View,
        startX: Float,
        startY: Float,
        endX: Float,
        endY: Float,
        duration: Long,
        isRelative: Boolean = true,
        animationListener: AnimationListener? = null
    ) {
        val type = if (isRelative) Animation.RELATIVE_TO_SELF else Animation.ABSOLUTE
        val translateAnimation = TranslateAnimation(
            type,
            startX,
            type,
            startY,
            type,
            endX,
            type,
            endY
        )
        translateAnimation.duration = duration
        translateAnimation.fillAfter = true
        if (animationListener != null) {
            translateAnimation.setAnimationListener(animationListener)
        }
        view.startAnimation(translateAnimation)
    }

    public fun scale(
        view: View,
        fromX: Float, toX: Float, fromY: Float, toY: Float,
        pivotXValue: Float, pivotYValue: Float,
        duration: Long,
        isRelative: Boolean = true,
        animationListener: AnimationListener? = null
    ) {
        val type = if (isRelative) Animation.RELATIVE_TO_SELF else Animation.ABSOLUTE
        val scaleAnimation = ScaleAnimation(
            fromX, toX, fromY, toY, type, pivotXValue, type, pivotYValue
        )
        scaleAnimation.duration = duration
        scaleAnimation.fillAfter = true
        if (animationListener != null) {
            scaleAnimation.setAnimationListener(animationListener)
        }
        view.startAnimation(scaleAnimation)
    }

}