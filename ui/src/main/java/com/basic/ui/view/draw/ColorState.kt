package com.basic.ui.view.draw

import android.R
import android.content.res.ColorStateList

/**
 * @author Peter Liu
 * @since 2023/7/17 23:20
 *
 */
object ColorState {
    private val ENABLED_CHECKED_STATES = arrayOf(
        intArrayOf(R.attr.state_enabled, R.attr.state_checked),
        intArrayOf(
            R.attr.state_enabled, -R.attr.state_checked
        ),
        intArrayOf(-R.attr.state_enabled, R.attr.state_checked),
        intArrayOf(-R.attr.state_enabled, -R.attr.state_checked)
    )

    private val state_normal = intArrayOf() //常态
    const val state_enabled = android.R.attr.state_enabled
    const val state_focused = android.R.attr.state_focused
    const val state_checked = android.R.attr.state_checked
    const val state_selected = android.R.attr.state_selected
    const val state_pressed = android.R.attr.state_pressed
    const val state_activated = android.R.attr.state_activated
    const val state_active = android.R.attr.state_active


    fun create(vararg stateColorPair: Pair<IntArray, Int>): ColorStateList {
        val states = arrayOfNulls<IntArray>(stateColorPair.size)
        val colors = IntArray(stateColorPair.size)
        stateColorPair.forEachIndexed { index, pair ->
            states[index] = pair.first
            colors[index] = pair.second
        }
        return ColorStateList(states, colors)
    }


}

