package com.basic.ui.view.image.monitor

import android.graphics.drawable.Drawable
import android.widget.ImageButton
import android.widget.ImageView
import com.basic.env.App
import com.basic.ui.view.image.GlideRequest
import java.io.Serializable
import java.nio.ByteBuffer

/**
 *  //todo will imp this later
 *
 * 图片监控：包括异常上报和本地异常展示
 */
object ImageMonitor {

    //开关： 是否显示监控结果的状态
    var showDebugMonitorState = false

    fun monitorResult(
        byteBuffer: ByteBuffer?,
        startTime: Long,
        endTime: Long,
        length: Int,
        url: String?,
        success: Boolean,
        msg: String
    ) {
        //todo will imp this

    }

    fun checkDebugMonitorState(glideRequest: GlideRequest<Drawable>, imageView: ImageView) {
        if (App.isDev() && showDebugMonitorState) {
            glideRequest.addListener(DebugMonitorStateRequestListener(imageView))
        }
    }

    fun getMonitorResult(model: Any): MonitorResult? {
        return null
    }

    class MonitorResult : Serializable {
        var url: String? = null
        var fileType //文件类型 1-图片，2-视频，3-音频
                = 0
        var downloadDuration //下载时长
                : Long = 0
        var width //宽
                = 0
        var height //高
                = 0
        var length //大小
                : Long = 0
        var isGif //是否是动图
                = false
    }
}