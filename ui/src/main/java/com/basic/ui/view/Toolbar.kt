package com.basic.ui.view

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.basic.ui.R
import com.basic.ui.databinding.BasicUiLayoutToobarBinding
import com.basic.ui.dp
import com.basic.ui.resColor
import com.basic.ui.resFont
import com.basic.ui.resPx
import com.basic.ui.resString
import com.basic.ui.utils.OnViewClick
import com.basic.ui.utils.StatusBarHelper
import com.basic.ui.utils.setOnClick

/**
 * Toolbar
 * 默认: 左边箭头back, 右边竖三点更多, 中间大标题和子标题
 * 简单自定义:左边iconfont, 右边iconfont, 添加右边iconfont, 设置中间大标题和子标题
 * 深度自定义 左边view,中间view, 右边view.
 *
 * @author: Peter Liu
 * @date: 2022/11/9
 *
 */

class Toolbar @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : androidx.appcompat.widget.Toolbar(context, attrs, defStyleAttr) {

    private val binding: BasicUiLayoutToobarBinding

    init {
        binding = BasicUiLayoutToobarBinding.inflate(LayoutInflater.from(context), this, true)
    }

    fun enableLeftBack(activity: Activity) {
        binding.tvLeft.isVisible = true
        activity.onBackPressed()
    }

    fun enableLeftBack(fragment: Fragment) {
        binding.tvLeft.isVisible = true
        fragment.activity?.onBackPressed()
    }

    fun setLeftIconfont(@StringRes iconfont: Int, onViewClick: OnViewClick) {
        binding.tvLeft.apply {
            setOnClick(onClick = onViewClick)
            text = iconfont.resString()
        }
    }

    fun setRightIconfont(@StringRes iconfont: Int, onViewClick: OnViewClick) {
        binding.tvRight.apply {
            setOnClick(onClick = onViewClick)
            text = iconfont.resString()
        }
    }

    fun addRightIconfont(@StringRes iconfont: Int, onViewClick: OnViewClick) {
        addRightView(TextView(context) {
            h = LayoutParams.MATCH_PARENT
            typeface = R.font.iconfont.resFont()
            text = iconfont.resString()
            setTextColor(R.color.black.resColor())
            setTextSize(TypedValue.COMPLEX_UNIT_PX, R.dimen.sp_28.resPx().toFloat())
            val padding = 10.dp
            textAlignment = TEXT_ALIGNMENT_CENTER
            setPadding(padding, 0, padding, 0)
        })
    }

    fun setTitle(title: String) {
        binding.tvTitle.isVisible = true
        binding.tvTitle.text = title
    }

    fun setSubTitle(subTitle: String) {
        binding.tvSubTitle.isVisible = true
        binding.tvSubTitle.text = subTitle
    }

    fun setLeftView(view: View) {
        binding.tvLeft.gone()
        binding.groupLeft.addView(view)
    }

    fun setRightView(view: View) {
        binding.tvRight.gone()
        binding.groupRight.addView(view)
    }

    /**
     * 后add的view在右边
     */
    fun addRightView(view: View) {
        binding.groupRight.addView(view)
    }

    fun setCenterView(view: View) {
        binding.tvTitle.gone()
        binding.tvSubTitle.gone()
        binding.groupCenter.addView(view)
    }

    fun setTranslucent(isTrue: Boolean) {
        setPadding(0, if (isTrue) StatusBarHelper.getStatusBarHeight(context) else 0, 0, 0)
    }
}