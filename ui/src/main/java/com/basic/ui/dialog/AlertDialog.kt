package com.basic.ui.dialog

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import com.basic.env.AppLifecycleManager
import com.basic.ui.R
import com.basic.ui.isAvailable
import com.basic.ui.resString
import com.basic.ui.view.getActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder

/**
 * @author Peter Liu
 * @since 2023/8/10 00:14
 *
 */
object Alert {

    private fun create(
        context: Context,
        title: String? = null,
        message: String? = null,
        ok: String? = null,
        onClickOk: OnDialogClick? = null,
        cancel: String? = null,
        onClickCancel: OnDialogClick? = {
            it.dismiss()
        }
    ): AlertDialog {
        val activity = context.getActivity() ?: AppLifecycleManager.getPresentActivity()
        ?: throw IllegalArgumentException("context is not An Activity")
        val dialogBuilder =
            MaterialAlertDialogBuilder(activity);
        if (!title.isNullOrEmpty()) {
            dialogBuilder.setTitle(title)
        }
        if (!message.isNullOrEmpty()) {
            dialogBuilder.setMessage(message)
        }
        if (!ok.isNullOrEmpty()) {
            dialogBuilder.setPositiveButton(ok) { dialog, which ->
                onClickOk?.invoke(dialog)
            }
        }
        if (!cancel.isNullOrEmpty()) {
            dialogBuilder.setNegativeButton(cancel) { dialog, which ->
                onClickCancel?.invoke(dialog)
            }
        }

        return dialogBuilder.create()
    }

    /**
     * 2个按钮, 默认"确定", "取消"
     */
    fun createTwoButton(
        context: Context,
        title: String? = null,
        message: String? = null,
        ok: String? = R.string.basic_ui_confirm.resString(),
        onClickOk: OnDialogClick? = {
            it.dismiss()
        },
        cancel: String? = R.string.basic_ui_cancel.resString(),
        onClickCancel: OnDialogClick? = {
            it.dismiss()
        }
    ): AlertDialog {
        return create(context, title, message, ok, onClickOk, cancel, onClickCancel)
    }

    /**
     * 一个按钮, 默认"确定"
     */
    fun createPositive(
        context: Context,
        title: String? = null,
        message: String,
        ok: String = R.string.basic_ui_confirm.resString(),
        onOkClick: OnDialogClick = {
            it.dismiss()
        }
    ): AlertDialog {
        return create(context, title, message, ok, onOkClick, null, null)
    }


    /**
     * 一个按钮, 默认"取消"
     */
    fun createNegative(
        context: Context,
        title: String? = null,
        message: String,
        cancel: String = R.string.basic_ui_cancel.resString(),
        onClickCancel: OnDialogClick = {
            it.dismiss()
        }
    ): AlertDialog {
        return create(context, title, message, null, null, cancel, onClickCancel)
    }

    /**
     * 没有按钮
     */
    fun createNoneButton(
        context: Context,
        title: String? = null,
        message: String,
    ): AlertDialog {
        return create(context, title, message, null, null, null, null)
    }

}

fun Dialog.showSafely() {
    if (this.context.getActivity().isAvailable()) {
        this.show()
    }
}


typealias OnDialogClick = (DialogInterface) -> Unit




