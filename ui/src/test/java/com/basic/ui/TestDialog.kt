package com.basic.ui

import android.content.Context
import com.basic.ui.dialog.Alert
import com.basic.ui.dialog.showSafely

class TestDialog {
    fun test(context: Context) {
        Alert.createTwoButton(context, "a", "ss").apply {
            setCancelable(false)
            setCanceledOnTouchOutside(false)
            setOnShowListener {

            }
        }.showSafely()

        Alert.createPositive(context, message = "ss", ok = "ok") {
        }.showSafely()
        Alert.createPositive(context, message = "ss") {

        }.showSafely()
        Alert.createPositive(context, "a", "ss", "ok") {

        }.showSafely()

        Alert.createNegative(context, message = "ss").showSafely()
        Alert.createNegative(context, message = "ss") {

        }.showSafely()
        Alert.createNegative(context, "a", "ss", "back").showSafely()
    }

}